name := "scalaq"

organization := "th.ac.ku.lcac"

version := "0.1.0"

scalaVersion := "2.9.1"

libraryDependencies ++= Seq(
    "org.apache.commons" % "commons-math" % "2.0",
    "org.slf4j" % "slf4j-api" % "1.6.0",
    "ch.qos.logback" % "logback-core" % "0.9.24",
    "ch.qos.logback" % "logback-classic" % "0.9.24",
    "org.scalatest" %% "scalatest" % "1.8" % "test"
)
