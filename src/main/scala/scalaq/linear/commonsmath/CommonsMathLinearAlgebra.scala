package scalaq.linear.commonsmath

import scalaq.linear.{LinearAlgebra, Matrix}
import org.apache.commons.math.linear.{EigenDecompositionImpl}

class CommonsMathLinearAlgebra extends LinearAlgebra {

    import CommonsMathMatrix._

    def zeroes(i: Int, j: Int): Matrix = new CommonsMathMatrix(i, j)

    def eigenValueDecomposition(m: Matrix): (Matrix, Array[Double]) = {
        val evd = new EigenDecompositionImpl(m, 1.0E-11)
        val eigenPairs = for (i <- 0 until m.getColumnDimension) 
            yield (evd.getRealEigenvalue(i), evd.getEigenvector(i))

        val sortEigenPairs = eigenPairs.toList.sortWith((eigen1, eigen2) => eigen1._1 < eigen2._1)

        val ncols = m.getColumnDimension
        val nrows = m.getRowDimension
        val eigenVectors = new CommonsMathMatrix(nrows, ncols)
        val eigenValues = new Array[Double](ncols)
        for {
            col <- 0 until ncols
            eigen = sortEigenPairs(col)
            row <- 0 until nrows
        } {
           eigenVectors.addToEntry(row, col, eigen._2.getEntry(row))
           if (col == row) {
               eigenValues(col) =  eigen._1
           }
        }

        (new CommonsMathMatrix(eigenVectors), eigenValues)
    }
}
