package scalaq.linear.commonsmath

import scalaq.linear.{Matrix}

import org.apache.commons.math.linear.{RealMatrix, Array2DRowRealMatrix, ArrayRealVector, EigenDecompositionImpl}

object CommonsMathMatrix {
    implicit def conv1(m: Matrix): RealMatrix = m.asInstanceOf[CommonsMathMatrix].m
    implicit def conv2(m: RealMatrix): Matrix = new CommonsMathMatrix(m)
}

class CommonsMathMatrix(val m: RealMatrix) extends Matrix {
    import CommonsMathMatrix._

    def this(i: Int, j: Int) = this(new Array2DRowRealMatrix(i, j))

    def apply(i: Int, j: Int): Double = m.getEntry(i, j)

    def update(i: Int, j: Int, value: Double): Unit = m.setEntry(i, j, value)

    def -(n: Matrix): Matrix = m subtract n

    def +(n: Matrix): Matrix = m add n

    def *(n: Matrix): Matrix = m multiply n

    def transpose = m.transpose

    def *(c: Double): Matrix = m scalarMultiply c

    def /(c: Double): Matrix = m scalarMultiply (1.0/c)

    def size: (Int, Int) = (m.getRowDimension, m.getColumnDimension)

    def subMatrix(startRow: Int, endRow: Int, startColumn: Int, endColumn: Int): Matrix = {
        m.getSubMatrix(startRow, endRow, startColumn, endColumn)
    }


}


