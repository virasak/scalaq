package scalaq.chemistry

import scalaq.linear.Vector

class Atom(val label: String, val element: Element, val position: Vector) {

    def this(element: Element, position: Vector) = this(element.toString, element, position)

    def moveTo(newPosition: Vector): Atom = new Atom(label, element, newPosition)

    def move(delta: Vector): Atom = new Atom(label, element, position + delta)

    def protons = element.atomicNumber

    override def toString: String = "%5s\t%s".format(label, position)

}

object Atom {
    def apply(label: String, element: Element, x: Double, y: Double, z: Double): Atom = new Atom(label, element, Vector(x, y, z))

    def apply(element: Element, x: Double, y: Double, z: Double): Atom = new Atom(element, Vector(x, y, z))

    def apply(label: String, element: Element): Atom = apply(label, element, 0, 0, 0)

    def apply(element: Element): Atom = apply(element, 0, 0, 0)
}
