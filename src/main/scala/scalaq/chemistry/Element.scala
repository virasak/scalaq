package scalaq.chemistry

/**
 * Chemical element data 
 */
sealed abstract class Element(val atomicNumber: Int, val atomicMass: Double)

// TODO
//   check more references for element data
//   add all current elements in the table
object Element {

    private val elist = List(
        H,                                                                  He,
        Li, Be,                                          B,  C,  N,  O,  F, Ne,
        Na, Mg,                                         Al, Si,  P,  S, Cl, Ar,
         K, Ca, Sc, Ti,  V, Cr, Mn, Fe, Co, Ni, Cu, Zn, Ga, Ge, As, Se, Br, Kr,
        Rb, Sr,  Y, Zr, Nb, Mo, Tc, Ru, Rh, Pd, Ag, Cd, In, Sn, Sb, Te,  I, Xe,
        Cs, Ba,     Hf, Ta,  W, Re, Os, Ir, Pt, Au, Hg, Tl, Pb, Bi, Po, At, Rn,
        Fr, Ra
    )

    def apply(name: String): Option[Element] = elist.find(_.toString == name)

    case object H  extends Element(  1, 1.00794)
    case object He extends Element(  2, 4.00260)

    case object Li extends Element(  3, 6.941)
    case object Be extends Element(  4, 9.01218)
    case object B  extends Element(  5, 10.81)
    case object C  extends Element(  6, 12.011)
    case object N  extends Element(  7, 14.0067)
    case object O  extends Element(  8, 15.9994)
    case object F  extends Element(  9, 18.998403)
    case object Ne extends Element( 10, 20.179)

    case object Na extends Element( 11, 22.98977)
    case object Mg extends Element( 12, 24.305)
    case object Al extends Element( 13, 26.98154)
    case object Si extends Element( 14, 28.0855)
    case object P  extends Element( 15, 30.97376)
    case object S  extends Element( 16, 32.06)
    case object Cl extends Element( 17, 35.453)
    case object Ar extends Element( 18, 39.948)

    case object K  extends Element( 19, 39.0983)
    case object Ca extends Element( 20, 40.08)
    case object Sc extends Element( 21, 44.9559)
    case object Ti extends Element( 22, 51.996)
    case object V  extends Element( 23, 47.88)
    case object Cr extends Element( 24, 50.9415)
    case object Mn extends Element( 25, 54.9380)
    case object Fe extends Element( 26, 55.847)
    case object Co extends Element( 27, 58.9332) // Why heavier than Ni?
    case object Ni extends Element( 28, 58.69)
    case object Cu extends Element( 29, 63.546)
    case object Zn extends Element( 30, 65.38)
    case object Ga extends Element( 31, 69.72)
    case object Ge extends Element( 32, 72.59)
    case object As extends Element( 33, 74.9216)
    case object Se extends Element( 34, 78.96)
    case object Br extends Element( 35, 79.904)
    case object Kr extends Element( 36, 83.80)

    case object Rb extends Element( 37, 85.4678)
    case object Sr extends Element( 38, 87.62)
    case object Y  extends Element( 39, 88.9059)
    case object Zr extends Element( 40, 91.22)
    case object Nb extends Element( 41, 92.9064)
    case object Mo extends Element( 42, 95.94)
    case object Tc extends Element( 43, 98)
    case object Ru extends Element( 44, 101.07)
    case object Rh extends Element( 45, 102.9055)
    case object Pd extends Element( 46, 106.42)
    case object Ag extends Element( 47, 107.8682)
    case object Cd extends Element( 48, 112.41)
    case object In extends Element( 49, 114.82)
    case object Sn extends Element( 50, 118.69)
    case object Sb extends Element( 51, 121.75)
    case object Te extends Element( 52, 127.60) // Why heavy than I?
    case object I  extends Element( 53, 126.9045)
    case object Xe extends Element( 54, 131.29)

    case object Cs extends Element( 55, 132.9054)
    case object Ba extends Element( 56, 137.33)

    case object Hf extends Element( 72, 178.49)
    case object Ta extends Element( 73, 180.9479)
    case object W  extends Element( 74, 183.85)
    case object Re extends Element( 75, 186.207)
    case object Os extends Element( 76, 190.2)
    case object Ir extends Element( 77, 192.22)
    case object Pt extends Element( 78, 195.08)
    case object Au extends Element( 79, 196.9665)
    case object Hg extends Element( 80, 200.59)
    case object Tl extends Element( 81, 204.383)
    case object Pb extends Element( 82, 207.2)
    case object Bi extends Element( 83, 208.9804)
    case object Po extends Element( 84, 209)
    case object At extends Element( 85, 210)
    case object Rn extends Element( 86, 222)


    case object Fr extends Element( 87, 223)
    case object Ra extends Element( 88, 226.0254)
}




