package scalaq.quantum.basis.gaussian

import scalaq.linear.Vector
import org.apache.commons.math.util.MathUtils.{factorial, binomialCoefficient}
import org.apache.commons.math.special.Gamma.{logGamma, regularizedGammaQ}
import scala.math.{pow, floor, abs, Pi, exp, sqrt}


object PGBF {

    def overlap(fA: PGBF, fB: PGBF): Double = fA.norm * fB.norm * overlap_(fA, fB)

    def overlap_(fA: PGBF, fB: PGBF): Double = {
        val gamma = fA.alpha + fB.alpha
        val doubleGamma = 2 * gamma
        val P = fA.productCenter(fB)
        val PA = P - fA.center
        val PB = P - fB.center

        val t1 = pow(Pi/gamma, 1.5)
        val t2 = exp(-fA.alpha * fB.alpha * fA.distance2(fB)/gamma)

        def sumTerm(lA: Int, lB: Int, pA: Double, pB: Double): Double = {
            var sum = 0.0

            for (k <- 0 to ((lA + lB)/2).toInt) {
                sum += fi(2 * k, lA, lB, pA, pB) * factorial2(2 * k - 1)/ pow(doubleGamma, k)
            }

            sum
        }

        val t3 = sumTerm(fA.l, fB.l, PA.x, PB.x) * sumTerm(fA.m, fB.m, PA.y, PB.y) * sumTerm(fA.n, fB.n, PA.z, PB.z)

        t1 * t2 * t3
    }


    def kinetic(fA: PGBF, fB: PGBF): Double = {
        val t1 = fB.alpha * (2 * fB.totalL + 3) * overlap_(fA, fB)
        val t2 = 2 * fB.alpha * fB.alpha * ( overlap_(fA, fB.shiftl(2)) + overlap_(fA, fB.shiftm(2)) + overlap_(fA, fB.shiftn(2)))
        val t3 = 0.5 * (
                     (fB.l * (fB.l - 1)) * overlap_(fA, fB.shiftl(-2)) +
                     (fB.m * (fB.m - 1)) * overlap_(fA, fB.shiftm(-2)) +
                     (fB.n * (fB.n - 1)) * overlap_(fA, fB.shiftn(-2))
                 )
        fA.norm * fB.norm * (t1 - t2 - t3)
    }

    def nuclear(fA: PGBF, fB: PGBF, nuclearCenter: Vector): Double = {
        val gamma = fA.alpha + fB.alpha
        val p = fA.productCenter(fB)
        val t1 = 2.0 * Pi / gamma
        val t2 = exp(-fA.alpha * fB.alpha * fA.distance2(fB) / gamma)

        def sumTerm(ax: Double, l1: Int,
                bx: Double, l2: Int,
                cx: Double, px: Double, gamma: Double): Array[Double] = {

            val pa = px - ax
            val pb = px - bx
            val pc = px - cx

            def aTerm(i: Int, r: Int, u: Int): Double = {

                pow(-1, i) *
                fi(i, l1, l2, pa, pb) *
                pow(pc, i - 2 * r - 2 * u) *
                pow(-1, u) *
                factorial(i) *
                pow(0.25/gamma, r + u) /
                (factorial(r) * factorial(u) * factorial(i - 2 * r - 2 * u))
            }

            val sum = new Array[Double](l1 + l2 + 1)

            for {
                i <- 0 to (l1 + l2)
                r <- 0 to (i/2)
                u <- 0 to ((i - 2 * r)/2)
                n = i - 2 * r - u
            } sum(n) += aTerm(i, r, u)

            sum
        }

        var t3 = 0.0
        val xTerms = sumTerm(fA.xc, fA.l, fB.xc, fB.l, nuclearCenter.x, p.x, gamma)
        val yTerms = sumTerm(fA.yc, fA.m, fB.yc, fB.m, nuclearCenter.y, p.y, gamma)
        val zTerms = sumTerm(fA.zc, fA.n, fB.zc, fB.n, nuclearCenter.z, p.z, gamma)
        val npSq = p.distance2(nuclearCenter)

        for (i <- 0 until xTerms.size; j <- 0 until yTerms.size; k <- 0 until zTerms.size ) {
            t3 += xTerms(i) * yTerms(j) * zTerms(k) * fgamma(i+j+k, npSq * gamma)
        }
        fA.norm * fB.norm * t1 * t2 * t3
    }

    def coulomb(f1: PGBF, f2: PGBF, f3: PGBF, f4: PGBF): Double = {

        val p = f1.productCenter(f2)
        val q = f3.productCenter(f4)
        val pqSq = p.distance2(q)
        val gamma1 = f1.alpha + f2.alpha
        val gamma2 = f3.alpha + f4.alpha
        val delta = 0.25 / gamma1 + 0.25 / gamma2
        val t1 = 2.0 * pow(Pi, 2.0) / (gamma1 * gamma2)
        val t2 = pow(Pi / (gamma1 + gamma2), 0.5)
        val t3 = exp(-f1.alpha * f2.alpha * f1.distance2(f2)/gamma1 - f3.alpha * f4.alpha * f3.distance2(f4)/gamma2)
        var t4 =  0.0

        def sumTerm(x1: Double, l1: Int,
                    x2: Double, l2: Int,
                    x3: Double, l3: Int,
                    x4: Double, l4: Int,
                    px: Double, qx: Double) = {

            def bTerm(i1: Int, i2: Int, r1: Int, r2: Int, u: Int): Double = {
                val t1 = pow(-1, i2)
                val t2 = fi(i1, l1, l2, px - x1, px - x2)
                val t3 = fi(i2, l3, l4, qx - x3, qx - x4)
                val t4 = factorial(i1) * factorial(i2)
                val t5 = pow(4 * gamma1, i1) * pow(4 * gamma2, i2) * pow(delta, i1 + i2)
                val t6 = pow(4 * gamma1, r1) * pow(4 * gamma2, r2) * pow(delta, r1 + r2)
                val t7 = factorial(r1) * factorial(r2) * factorial(i1 - 2 * r1) * factorial(i2 - 2 * r2)
                val t8 = factorial(i1 + i2 - 2 * (r1 + r2))
                val t9 = pow(-1, u) * pow(qx - px, i1 + i2 - 2 * (r1 + r2) - 2 * u) * pow(delta, u)
                val t10= factorial(u) * factorial(i1 + i2 - 2 * (r1 + r2) - 2 * u)

                t1 * t2 * t3 * t4 * t6 * t8 * t9 / (t5 * t7 * t10)
            }

            val sum = new Array[Double](l1 + l2 + l3 + l4 + 1)
            for {
                i1 <- 0 to (l1 + l2)
                i2 <- 0 to (l3 + l4)
                r1 <- 0 to (i1/2)
                r2 <- 0 to (i2/2)
                u  <- 0 to ((i1 + i2)/2 - r1 - r2)
                i = i1 + i2 - 2 * (r1 + r2) - u
            } sum(i) += bTerm(i1, i2, r1, r2, u)

            sum
        }

        val xTerms = sumTerm(f1.xc, f1.l, f2.xc, f2.l, f3.xc, f3.l, f4.xc, f4.l, p.x, q.x)
        val yTerms = sumTerm(f1.yc, f1.m, f2.yc, f2.m, f3.yc, f3.m, f4.yc, f4.m, p.y, q.y)
        val zTerms = sumTerm(f1.zc, f1.n, f2.zc, f2.n, f3.zc, f3.n, f4.zc, f4.n, p.z, q.z)

        for {
            i <- 0 until xTerms.size
            j <- 0 until yTerms.size
            k <- 0 until zTerms.size
        } t4 += xTerms(i) * yTerms(j) * zTerms(k) * fgamma(i + j + k, pqSq / (4 * delta))

        f1.norm * f2.norm * f3.norm * f4.norm * t1 * t2 * t3 * t4
    }




    def fgamma(g: Double, t: Double): Double = {
        if (g <= -0.5) {
            throw new RuntimeException("fgamma is not defined for m <= -1/2")
        } else if (t < 1E-6) {
            1 / (2 * g + 1)
        } else {
            val x = g + 0.5
            0.5 * pow(t, -x) * exp(logGamma(x)) * ( 1 - regularizedGammaQ(x, t))
        }
    }

    /**
     * coeficient of x^i in (x - a)^la * (x - b)^lb
     */
    def fi(i: Int, la: Int, lb: Int, a: Double, b: Double): Double = {
        for ( j <- 0 to la; k <- 0 to lb if (la - j) + (lb - k) == i)
            yield pow(a,j) * pow(b, k) * binomialCoefficient(la, j) * binomialCoefficient(lb, k)
    }.foldLeft(0.0)(_+_)

    /**
     * Double factorial
     */
    def factorial2(n: Int): Int = {
        def fact2(n: Int, acc: Int): Int =
            if (n <= 1)
                acc
            else
                fact2(n - 2, n * acc)

        fact2(n, 1)
    }
}

class PGBF(val l: Int, val m: Int, val n: Int, val center: Vector, val alpha: Double) {

    import PGBF.factorial2

    def apply(position: Vector): Double = {
        val dr = position - center

        pow(dr.x, l) * pow(dr.y, m) * pow(dr.z, n) * exp(alpha * position.distance2(center))
    }

    def orbitalName = (l, m, n) match {
        case (0, 0, 0) => "S"

        case (1, 0, 0) => "Px"
        case (0, 1, 0) => "Py"
        case (0, 0, 1) => "Pz"

        case (2, 0, 0) => "Fx^2"
        case (0, 2, 0) => "Fy^2"
        case (0, 0, 2) => "Fz^2"
        case (1, 1, 0) => "Fxy"
        case (1, 0, 1) => "Fxz"
        case (0, 1, 1) => "Fzx"

        case (l, m, n) => "(%d, %d, %d)".format(l, m, n)
    }

    override def toString = {
        "%5s center=%s alpha=%12.6f".format(orbitalName, center, alpha)
    }

    lazy val norm: Double = {

        if (abs(alpha) < 1e-8) {
            1.0
        } else {
            sqrt(
                (pow(2, 2 * totalL + 1.5) *
                 pow(alpha, totalL + 1.5))
                /
                (factorial2(2 * l - 1) *
                 factorial2(2 * m - 1) *
                 factorial2(2 * n - 1) *
                 pow (Pi, 1.5))
            )
        }
    }

    def productCenter(other: PGBF): Vector =
        ((center * alpha + other.center * other.alpha)
         /
         (alpha + other.alpha))

    def totalL = l + m + n

    def xc = center.x

    def yc = center.y

    def zc = center.z

    def distance2(other: PGBF): Double = center.distance2(other.center)

    def shiftl(s: Int): PGBF = new PGBF(l + s, m    , n    , center, alpha)

    def shiftm(s: Int): PGBF = new PGBF(l    , m + s, n    , center, alpha)

    def shiftn(s: Int): PGBF = new PGBF(l    , m    , n + s, center, alpha)
}

