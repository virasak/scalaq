package scalaq.quantum.basis.gaussian

import scala.io.Source
import scala.util.parsing.combinator._
import scalaq.chemistry.{Element, Atom, Molecule}
import scalaq.util.parsing.LineOrientedParsers
import scalaq.quantum.{BasisSetLoader, BasisSet}


class G94BasisSetLoader extends BasisSetLoader {

    type LMN = (Int,Int,Int)

    type PGF = (Double,Double)


    def toURL(basisName: String) = {
        val fileName = "/basis/gaussian94/%s.bas".format(basisName)
        getClass.getResource(fileName)
    }

    def isDefinedAt(basisName: String): Boolean = {
        toURL(basisName) != null
    }

    def load(basisName: String, molecule: Molecule): BasisSet = {
        val src = Source.fromURL(toURL(basisName))
        val srcCharSeq = src.getLines.
                             dropWhile(line => !(line startsWith "****")).
                             drop(1).
                             mkString

        val g94parser = new G94Parsers

        val g94data: List[(Element,List[List[(LMN,Double,PGF)]])] =
            g94parser.parseAll(g94parser.all, srcCharSeq).
                      getOrElse (throw new RuntimeException("Parse error"))

        val cgbfs = molecule.atoms.flatMap { atom =>
            val elementData = g94data filter { case (e,_) => e == atom.element } map { x => x._2 }

            elementData.map {
                cgbfData => cgbfData.map { cgbf => new CGBF(
                    cgbf.map { case (lmn, norm, (coeff, alpha)) =>
                        (coeff, new PGBF(lmn._1, lmn._2, lmn._3, atom.position, alpha))
                    })
                }
            }
        }

        new ContractedGaussianBasisSet(basisName, cgbfs.flatten)
    }


    class G94Parsers extends LineOrientedParsers {

        def doubleValue: Parser[Double] = """[\+\-]?\d+\.\d+""".r ^^ (_.toDouble)

        def intValue: Parser[Int] = """\d+""".r ^^ (_.toInt)

        def newLine: Parser[String] = "\n"

        def orbital: Parser[List[List[LMN]]] = """\w+""".r ^^ { sym => sym.toList.map(lmn _) }

        def lmn(symbol: Char): List[LMN] = symbol match {
            case 'S' => List((0, 0, 0))
            case 'P' => List((1, 0, 0), (0, 1, 0), (0, 0, 1))
            case 'D' => List((2,0,0), (0,2,0), (0,0,2), (1,1,0), (0,1,1), (1,0,1))
            case _   => Nil
        }

        def sectionTerminator =  "****" ~ newLine

        def all: Parser[List[(Element,List[List[(LMN,Double,PGF)]])]] = repsep(element ~ shells, sectionTerminator) <~ "****" <~ opt(rep(newLine)) ^^ {
            xs => xs.map {
                case e ~ sh => (e, sh)
            }
        }

        def element: Parser[Element] = elementType <~ "0" <~ newLine

        def elementType: Parser[Element] = """\w+""".r ^^ { t =>
            Element(t) match {
                case Some(e) => e
                case None => throw new RuntimeException("Not found %s element".format(t))
            }
        }

        def shells: Parser[List[List[(LMN,Double,PGF)]]] = rep(shell) ^^ (_ flatten)

        def shell: Parser[List[List[(LMN,Double,PGF)]]] = orbital ~ intValue ~ doubleValue ~ newLine ~ rep(primitive) ^^ {
            case orbs ~ n ~ norm ~ s ~ prims =>
                orbs.zipWithIndex.flatMap { case (orb, index) =>
                    orb.map { o =>
                        prims.map { prim => (o, norm, prim(index)) }
                    }
                }
        }

        def primitive: Parser[List[PGF]] = rep(doubleValue) <~ newLine ^^ {
            case Nil => Nil
            case alpha::coefs => coefs.map { coef => (coef, alpha)}
        }

    }
}
