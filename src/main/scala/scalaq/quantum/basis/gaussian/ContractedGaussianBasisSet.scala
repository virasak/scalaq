package scalaq.quantum.basis.gaussian

import scalaq.chemistry.Atom
import scalaq.linear.Vector
import scalaq.quantum.{BasisSet}

class ContractedGaussianBasisSet(val name: String, val bfs: List[CGBF]) extends BasisSet {

    def size = bfs.length

    def centerOf(i: Int): Vector =  bfs(i).center

    def prob(i: Int, r: Vector): Double = bfs(i)(r)

    def overlap(i: Int, j: Int): Double = overlap(bfs(i), bfs(j))

    def overlap(fi: CGBF, fj: CGBF): Double = fi.norm*fj.norm*(fi overlap fj)

    def kinetic(i: Int, j: Int): Double = kinetic(bfs(i), bfs(j))

    def kinetic(fi: CGBF, fj: CGBF): Double = fi.norm*fj.norm*(fi kinetic fj)

    def nuclear(atom: Atom, i: Int, j: Int): Double = nuclear(atom, bfs(i), bfs(j))

    def nuclear(atom: Atom, fi: CGBF, fj: CGBF): Double = -atom.protons*fi.norm*fj.norm*(fi nuclear (fj, atom.position))

    def coulomb(i: Int, j: Int, k: Int, l: Int): Double = coulomb(bfs(i), bfs(j), bfs(k), bfs(l))

    def coulomb(fi: CGBF, fj: CGBF, fk: CGBF, fl: CGBF): Double =
        fi.norm*fj.norm*
        fk.norm*fl.norm*
        (fi coulomb (fj,fk,fl))


    override def toString: String = (name::bfs.map(_.toString)).mkString("\n")
}
