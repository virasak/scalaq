package scalaq.quantum.hf

import scalaq.chemistry.{Molecule, Atom}
import scalaq.linear.Matrix
import scalaq.linear.Matrix._
import scalaq.quantum.{BasisSet}
import HartreeFock._
import scala.math.{abs, sqrt}


object RestrictedHartreeFock {
    val logger = org.slf4j.LoggerFactory.getLogger(this.getClass)
}
/**
 * Restricted Hartree Fock method (RHF)
 */
class RestrictedHartreeFock(charges: Int, basisSet: BasisSet, molecule: Molecule)
    extends HartreeFock(charges, 1, basisSet, molecule) {

    require(nOccupiedOrbitals * 2 == molecule.electrons(charges))

    /**
     *
     */
    def this(charges: Int, basisName: String, molecule: Molecule) =
        this(charges, BasisSet.load(basisName, molecule), molecule)


    /**
     * Self Consistent Field calculation for Hartree Fock Method
     */
    def start(maxLoop: Int, epsilon: Double)(statusCallBack: Status => Unit): Unit = {
        logger.debug("Start RHF (maxloop={} epsilon={}", maxLoop, epsilon)
        val x = xMatrix
        val xt = x.transpose
        val h = hMatrix


        def scf(currentLoop: Int, prevP: Matrix, prevE0: Double): Unit = {
            logger.debug("Iteration #{}", currentLoop)

            statusCallBack(StartLoop(currentLoop))

            val f = h + gMatrix(prevP)

            // solve secular equation
            val (cp, energy) = eigenValueDecomposition((xt * f * x).forceSymmetric)
            logger.debug("Energy eigenvalues {}", energy)
            val c = x * cp

            // contruct densityMatrix
            val nextP = densityMatrix(c)
            logger.debug("Density matrix {}", nextP)

            val e0 = electronicEnergy(nextP, h, f)
            val deltaE0 = abs(e0 - prevE0)
            logger.debug("Delta E0 = {}", deltaE0)

            val delta = (nextP - prevP).elements
            val maxDelta = delta.map(abs _).reduceLeft { (x, y) => if (x > y) x else y }
            val rmsDelta = sqrt(delta.map(x => x * x).reduceLeft(_+_))
            logger.debug("RMS Delta P = {}", rmsDelta)
            logger.debug("MAX Delta P = {}", maxDelta)


            if (rmsDelta < 1E-4 && maxDelta < 1E-2 && deltaE0 < 1E-6) {
                logger.info("SCF is converted at iteration {}", currentLoop)
                statusCallBack(Finish(currentLoop, e0, energy, None, nextP, None))
            } else if (currentLoop == maxLoop) {
                logger.info("Reach maximum loop limit (max loop = {})", maxLoop)
                statusCallBack(ReachMaxLoop(currentLoop, energy, None, nextP, None))
            } else {
                logger.info("End of iteration {}", currentLoop)
                statusCallBack(EndLoop(currentLoop, e0))
                scf(currentLoop + 1, nextP, e0)
            }
        }

        statusCallBack(Start(charges, multiplicity, basisSet, molecule, maxLoop, epsilon))

        scf(1, guessDensityMatrix, 0)
    }


    /**
     *
     *
     */
    def guessDensityMatrix = zeroes(basisSet.size)


    /**
     *
     */
    def gMatrix(p: Matrix): Matrix = fillSymmetric(basisSet.size) { (i, j) =>
        val jkij = for( k <- basisSet.indices; l <- basisSet.indices)
                       yield p(k,l) * (
                                basisSet.coulomb(i, j, k, l)
                                -
                                basisSet.coulomb(i, k, j, l) * 0.5
                             )

        jkij.reduceLeft(_+_)
    }

    /**
    * c is matrix of occupied orbital's coefficients
    */
    def densityMatrix(c: Matrix): Matrix = {
        val occupiedC = c.takeColumns(nOccupiedOrbitals)
        occupiedC * occupiedC.transpose * 2
    }

    lazy val nOccupiedOrbitals: Int = molecule.electrons(charges) / 2

}
