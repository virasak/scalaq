package scalaq.quantum.hf

import scalaq.chemistry.{Molecule, Atom}
import scalaq.linear.Matrix
import scalaq.quantum.BasisSet
import HartreeFock._
/**
 *
 *
 */
class UnrestrictedHartreeFock(charges: Int, multiplicity: Int, basisSet: BasisSet, molecule: Molecule)
    extends HartreeFock(charges, multiplicity, basisSet, molecule) {

    require((nOccupiedAlphaOrbitals + nOccupiedBetaOrbitals) != molecule.electrons(charges))

    def this(charges: Int, multiplicity: Int, basisName: String, molecule: Molecule) = this(charges, multiplicity, BasisSet.load(basisName, molecule), molecule)

    /**
     * Self Consistent Field calculation for Hartree Fock Method
     */
    def start(maxLoop: Int, epsilon: Double)(statusCallBack: Status => Unit): Unit = {
        val x = xMatrix
        val xt = x.transpose
        val h = hMatrix
        val eNN = nuclearNuclearEnergy


        def iterate(pt: Matrix, ps: Matrix, remainedLoop: Int, lastEnergy: Double): Unit = {
            val currentLoop = maxLoop - remainedLoop

            statusCallBack(StartLoop(currentLoop))

            val fa = h + gAlphaMatrix(pt, ps)
            // solve secular equation
            val (cap, energya) = Matrix.eigenValueDecomposition(xt * fa * x)
            val ca = x * cap


            val fb = h + gBetaMatrix(pt, ps)
            // solve secular equation
            val (cbp, energyb) = Matrix.eigenValueDecomposition(xt * fb * x)
            val cb = x * cbp

            // contruct densityMatrix
            val (ptNew, psNew)= densityMatrix(ca, cb)

            val enEs = for (i <- basisSet.indices; j <- basisSet.indices)
                          yield ( ptNew(j,i) * (2 * h(i,j) + fa(i,j) + fb(i,j))
                                + psNew(j,i) * (fa(i,j) - fb(i,j)))

            val enE = 0.25 * enEs.foldLeft(0.0)(_+_)



            if (math.abs(enE - lastEnergy) < epsilon) {
                statusCallBack(Finish(currentLoop, enE, energya, Some(energyb), ptNew, Some(psNew)))
            } else if (remainedLoop == 0) {
                statusCallBack(ReachMaxLoop(currentLoop, energya, Some(energyb), ptNew, Some(psNew)))
            } else {
                statusCallBack(EndLoop(currentLoop, enE))
                iterate(ptNew, psNew, remainedLoop - 1, enE)
            }

        }

        statusCallBack(Start(charges, multiplicity, basisSet, molecule, maxLoop, epsilon))

        iterate(guessTotalDensityMatrix, guessSpinDensityMatrix, maxLoop - 1, 0.0)
    }



    /**
     *
     *
     */
    def guessTotalDensityMatrix = Matrix.zeroes(basisSet.size)
    def guessSpinDensityMatrix = Matrix.zeroes(basisSet.size)


    /**
     *
     */
    def gAlphaMatrix(pt: Matrix, ps: Matrix): Matrix = Matrix.fillSymmetric(basisSet.size) { (i, j) =>
        val jkij = for( k <- basisSet.indices; l <- basisSet.indices)
                       yield pt(k,l) * basisSet.coulomb(i, j, k, l) - 0.5 * (pt(k,l) + ps(k,l)) * basisSet.coulomb(i, k, j, l)

        jkij.reduceLeft(_+_)
    }

    /**
     *
     */
    def gBetaMatrix(pt: Matrix, ps: Matrix): Matrix = Matrix.fillSymmetric(basisSet.size) { (i, j) =>
        val jkij = for( k <- basisSet.indices; l <- basisSet.indices)
                       yield pt(k,l) * basisSet.coulomb(i, j, k, l) - 0.5 * (pt(k,l) - ps(k,l)) * basisSet.coulomb(i, k, j, l)

        jkij.reduceLeft(_+_)
    }

    def densityMatrix(ca: Matrix, cb: Matrix): (Matrix, Matrix) = {
        val occupiedCa = ca.takeColumns(nOccupiedAlphaOrbitals)
        val occupiedCb = cb.takeColumns(nOccupiedBetaOrbitals)
        val pa = occupiedCa * occupiedCa.transpose
        val pb = occupiedCb * occupiedCb.transpose
        (pa + pb, pa - pb)
    }

    lazy val nOccupiedSpinOrbitals: (Int, Int) = {
        val nOpenShells = multiplicity - 1
        val nClosedShells = (molecule.electrons(charges) - nOpenShells) / 2
        val nOccupiedAlphaOrbitals = nClosedShells + nOpenShells
        val nOccupiedBetaOrbitals  = nClosedShells

        (nOccupiedBetaOrbitals, nOccupiedBetaOrbitals)
    }

    val nOccupiedAlphaOrbitals = nOccupiedSpinOrbitals._1
    val nOccupiedBetaOrbitals  = nOccupiedSpinOrbitals._2

}
