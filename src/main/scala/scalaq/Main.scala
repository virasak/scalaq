package scalaq

import scala.io.Source
import quantum.hf.HartreeFock
import org.slf4j.LoggerFactory

object Main {

    val logger = LoggerFactory.getLogger(this.getClass)

    def main(args: Array[String]) {
        if (args.size > 1) {
            println("Usage scalaq inputfile")
        } else {
            val filename = args(0)
            logger.info("finding config file {}", filename)
            val inputSource = Source.fromFile(filename)
            logger.info("loading config file {}", filename)
            val config = Config.parse(inputSource)

            val hf = config.method.toUpperCase match {
                case "RHF" =>
                    logger.info("method is RHF")
                    HartreeFock.restrictedClosedShell(config.charge, config.basisName, config.molecule)
                case "UHF" =>
                    logger.info("method is UHF")
                    HartreeFock.unrestrictedOpenShell(config.charge, config.multiplicity, config.basisName, config.molecule)
                case other =>
                    logger.info("unknown method {}", other)
                    throw new RuntimeException("Unknown method: " + other)
            }

            hf.startWithWriter(new java.io.PrintWriter(System.out, true))
        }
    }
}
