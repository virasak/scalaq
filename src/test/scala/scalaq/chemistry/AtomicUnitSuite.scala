package scalaq.chemistry

import org.scalatest.{FunSuite, matchers}

import scalaq.chemistry.AtomicUnit._

class AtomicUnitSuite extends FunSuite with matchers.ShouldMatchers {

    val eps = 1E-11

    test ("length") {
        (0.52918 A) should be (1.0 plusOrMinus eps)
        (1.0 toA) should be (0.52918 plusOrMinus eps)
    }

    test ("energy") {
        (4.3598E-18 J) should be (1.0 plusOrMinus eps)
        (1.0 toJ) should be (4.3598E-18 plusOrMinus eps)
    }

    test ("mass") {
        (9.1095E-31 kg) should be (1.0 plusOrMinus eps)
        (1.0 toKg) should be (9.1095E-31 plusOrMinus eps)
    }

    test ("charge") {
        (1.6022E-19 C) should be (1.0 plusOrMinus eps)
        (1.0 toC) should be (1.6022E-19 plusOrMinus eps)
    }
}
