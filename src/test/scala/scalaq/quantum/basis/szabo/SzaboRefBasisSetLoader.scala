package scalaq.quantum.basis.szabo

import scalaq.chemistry.{Element, Atom, Molecule}
import scalaq.quantum.basis.gaussian.{PGBF, CGBF, ContractedGaussianBasisSet}
import scalaq.quantum.{BasisSetLoader, BasisSet}

class SzaboRefBasisSetLoader extends BasisSetLoader {
    val heZeta2 = math.pow(2.0925, 2)
    val hZeta2  = math.pow(1.24, 2)
    val sto3g = List(
            // (alpha, coefficient)
            (0.109818, 0.444635),
            (0.405771, 0.535328),
            (2.22766 , 0.154329)
    )


    val params = Map[Element,List[(Double,Double)]](

        Element.H  -> sto3g.map { case (alpha, coeff) => (alpha *  hZeta2, coeff) },

        Element.He -> sto3g.map { case (alpha, coeff) => (alpha * heZeta2, coeff) }
    )

    val supportedBasisSets = List("Szabo STO-3G")

    def isDefinedAt(id: String): Boolean = supportedBasisSets.exists(id == _)

    def load(id: String, molecule: Molecule): BasisSet = {

        val cgbfs = molecule.atoms.map { atom =>

            val pgbfs = params(atom.element).map { param =>
                    (param._2, new PGBF(0, 0, 0, atom.position, param._1))
            }.toList

            new CGBF(pgbfs)
        }

        new ContractedGaussianBasisSet(id, cgbfs)
    }
}
