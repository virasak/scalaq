package scalaq.quantum.basis
import scalaq.chemistry.{Element, Atom, Molecule}
import Element.{H,C, Sc}
import org.scalatest.{FunSuite, matchers}
import scalaq.quantum.basis.gaussian._
import scalaq.quantum.BasisSet
import scalaq.linear.Vector

class BasisSetLoaderSuite extends FunSuite with matchers.ShouldMatchers {

    val one = Vector(0, 0, 1)

    test ("load STO-3G basis set") {

        val h2Molecule = Molecule(Atom(H), Atom(H).move(one))

        val basisSet = BasisSet.load("STO-3G", h2Molecule)

        basisSet.size should be(2)
        basisSet.name should be("STO-3G")
    }

    test ("load 3-21G.bas from file") {
        // test S
        val h2Molecule = Molecule(Atom(H), Atom(H).move(one))
        val b321g = BasisSet.load("3-21G", h2Molecule)
        (b321g.size) should be(4)

        // test S SP
        val cMolecule = Molecule(Atom(C))
        val c321g = BasisSet.load("3-21G", cMolecule)
        (c321g.size) should be(9)

        // test D
        val scMolecule = Molecule(Atom(Sc))
        val sc321g = BasisSet.load("3-21G", scMolecule)
        (sc321g.size) should be (29)


        // test S SP for multiple atoms
        val ch2Molecule = Molecule(Atom(C), Atom(H).move(one), Atom(H).move(-one))
        val ch2321g = BasisSet.load("3-21G", ch2Molecule)
        (ch2321g.size) should be(13)
    }

    test ("load 3-21G.bas from file with Gaussian94BasisSetLoader") {
        val loader = new Gaussian94BasisSetLoader
        val h2Molecule = Molecule(Atom(H), Atom(H).move(one))
        val b321g = loader.load("3-21G", h2Molecule)
        (b321g.size) should be(4)

        // test S SP
        val cMolecule = Molecule(Atom(C))
        val c321g = loader.load("3-21G", cMolecule)
        (c321g.size) should be(9)

        // test D
        val scMolecule = Molecule(Atom(Sc))
        val sc321g = loader.load("3-21G", scMolecule)
        (sc321g.size) should be (29)


        // test S SP for multiple atoms
        val ch2Molecule = Molecule(Atom(C), Atom(H).move(one), Atom(H).move(-one))
        val ch2321g = loader.load("3-21G", ch2Molecule)
        (ch2321g.size) should be(13)

    }

}
