package scalaq.geometry

import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers
import scalaq.chemistry.Element
import scala.util.parsing.input._

class XYZParsersSuite extends FunSuite with ShouldMatchers {

    test ("methane input") {
        val in = new CharSequenceReader(
            """
            5
            methane molecule (comment)
            C 	 0.000000 	 0.000000 	 0.000000
            H 	 0.000000 	 0.000000 	 1.089000
            H 	 1.026719 	 0.000000 	-0.363000
            H 	-0.513360 	-0.889165 	-0.363000
            H 	-0.513360 	 0.889165 	-0.363000
            """.trim
        )

        val parser = new XYZParsers
        val methane = parser.all(in).get
        methane.atoms.size should be (5)
        (methane.label.trim) should be ("methane molecule (comment)")
        methane.atoms(0).element should be (Element.C)
        methane.atoms(1).element should be (Element.H)
        methane.atoms(2).element should be (Element.H)
        methane.atoms(3).element should be (Element.H)
        methane.atoms(4).element should be (Element.H)
    }
}
