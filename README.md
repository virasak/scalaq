Quantum Chemistry Calculation in Scala (ScalaQ)
===============================================

An *experimental project* to implement quantum chemistry calculation in Scala.

LICENSE
-------

This project is distributed in MIT license. Feel free to use it in your project.
